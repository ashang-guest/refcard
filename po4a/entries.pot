# XXX translation of the Debian reference card
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the refcard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: refcard 10.0\n"
"POT-Creation-Date: 2019-01-13 20:14+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING\n"

#. type: Attribute 'lang' of: <article>
#: entries.dbk:4
msgid "en-GB"
msgstr ""

#. type: Content of: <article><title>
#: entries.dbk:6
msgid "Debian Reference Card"
msgstr ""

#. type: Content of: <article><subtitle>
#: entries.dbk:9
msgid "The 101 most important things when using Debian"
msgstr ""

#. type: Content of: <article><articleinfo>
#: entries.dbk:11
msgid ""
"<copyright> <year>2004</year> <year>2010</year> <holder>W. Martin Borgert</"
"holder> </copyright> <copyright> <year>2016</year> <year>2019</year> "
"<holder>Holger Wansing</holder> </copyright> <copyright> <year>2019</year> "
"<holder>Your Name (\"Language\")</holder> </copyright>"
msgstr ""

#. type: Content of: <article><articleinfo><legalnotice><para>
#: entries.dbk:26
msgid ""
"This document may be used under the terms of the GNU General Public License "
"version 3 or higher. The license text can be found at <ulink url=\"https://"
"www.gnu.org/copyleft/gpl.html\">https://www.gnu.org/copyleft/gpl.html</"
"ulink> and <filename>/usr/share/common-licenses/GPL-3</filename>."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:33 entries.dbk:367
msgid "APT"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:34
msgid "Debian"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:35
msgid "dpkg"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:36
msgid "reference card"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:37
msgid "basic commands"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:38
msgid "Version"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:39
msgid "Made by"
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:43
msgid "Getting Help"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:46
msgid "man <replaceable>page</replaceable> or man bash"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:48
msgid "Read online help for every command and many configuration files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:52
msgid ""
"<replaceable>command</replaceable> &#x00A0; <optional><filename>--</"
"filename>help, <filename>-</filename>h</optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:55
msgid "Brief help for most commands."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:59
msgid ""
"<filename>/usr/share/doc/<optional><replaceable>package-name</replaceable>/</"
"optional></filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:60
msgid ""
"Find all documentation here, optional file <filename>README.Debian</"
"filename> contains specifics."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:65
msgid "<ulink url=\"https://www.debian.org/doc/\">Web documentation</ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:67
msgid ""
"Reference, manuals, FAQs, HOWTOs, etc. at <filename>https://www.debian.org/"
"doc/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:71
msgid ""
"<ulink url=\"https://lists.debian.org/\">Mailing lists</ulink> at "
"<filename>https://lists.debian.org/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:74
msgid "The community is always helpful, search for <filename>users</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:78
msgid ""
"<ulink url=\"https://wiki.debian.org/\">The Wiki</ulink> at "
"<filename>https://wiki.debian.org/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:80
msgid "Contains all kind of useful information."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:86
msgid "Installation"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:89
msgid ""
"<ulink url=\"https://www.debian.org/devel/debian-installer/\">Installer</"
"ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:91
msgid ""
"All information about it at <filename>https://www.debian.org/devel/debian-"
"installer/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:95
msgid "<ulink url=\"https://www.debian.org/distrib/\">CD images</ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:98
msgid "Download from <filename>https://www.debian.org/distrib/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:102
msgid "<filename>boot: expert</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:103
msgid "E.g. to set up the network w/o DHCP or using LILO instead of GRUB."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:109
msgid "Bugs"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:112
msgid ""
"<ulink url=\"https://bugs.debian.org/\">Bug Tracking</ulink> at "
"<filename>https://bugs.debian.org/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:115
msgid "All about existing and fixed bugs."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:119
msgid "Package specific"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:120
msgid ""
"See <filename>https://bugs.debian.org/<replaceable>package-name</"
"replaceable>/</filename>, use <filename>wnpp</filename> to ask for new "
"packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:126
msgid "reportbug"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:127
msgid "Report a bug by e-mail."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:130
msgid "<ulink url=\"https://www.debian.org/Bugs/Reporting\">Reporting</ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:132
msgid ""
"Instructions at <filename>https://www.debian.org/Bugs/Reporting</filename>"
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:138
msgid "Configuration"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:141
msgid "<filename>/etc/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:142
msgid ""
"All system configuration files are under directory <filename>/etc/</"
"filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:146
msgid "editor <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:147
msgid ""
"Default text editor.  May be <command>nano</command>, <command>emacs</"
"command>, <command>vi</command>, <command>joe</command>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:153
msgid ""
"<ulink url=\"http://localhost:631\">CUPS</ulink> at <filename>http://"
"hostname:631</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:155
msgid "Browser interface to printing system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:159
msgid "dpkg-reconfigure <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:161
msgid ""
"Reconfigure a package, e.g. <replaceable>keyboard-configuration</"
"replaceable> (keyboard), <replaceable>locales</replaceable> (localization)."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:165
msgid "update-alternatives <replaceable>options</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:167
msgid "Manage command alternatives."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:170
msgid "update-grub"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:171
msgid "After changing <filename>/etc/default/grub</filename>."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:177
msgid "Daemons and System"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:180
msgid ""
"<filename>systemctl restart <replaceable>name</replaceable>.service</"
"filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:182
msgid "Restart a service, system daemon."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:186
msgid ""
"<filename>systemctl stop <replaceable>name</replaceable>.service</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:188
msgid "Stop a service, system daemon."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:192
msgid ""
"<filename>systemctl start <replaceable>name</replaceable>.service</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:194
msgid "Start a service, system daemon."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:198
msgid "systemctl halt"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:199
msgid "Halts system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:202
msgid "systemctl reboot"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:203
msgid "Reboots system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:206
msgid "systemctl poweroff"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:207
msgid "Shuts down system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:210
msgid "systemctl suspend"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:211
msgid "Suspends system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:214
msgid "systemctl hibernate"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:215
msgid "Hibernates system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:218
msgid "<filename>/var/log/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:219
msgid "All log files are under this directory."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:223
msgid "<filename>/etc/default/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:224
msgid "Default values for many daemons and services."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:230
msgid "Important Shell Commands"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:233
msgid "cat <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:234
msgid "Print files to screen."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:237
msgid "cd <replaceable>directory</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:238
msgid "Change to directory."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:241
msgid "cp <replaceable>files</replaceable> <replaceable>dest</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:243
msgid "Copy files and directories."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:246
msgid "echo <replaceable>string</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:247
msgid "Echo string to screen."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:250
msgid ""
"gzip, bzip2, xz <optional><filename>-d</filename></optional> "
"<replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:253
msgid "Compress, uncompress files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:256
msgid "pager <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:257
msgid "Show contents of files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:260
msgid "ls <optional><replaceable>files</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:262
msgid "List files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:265
msgid "mkdir <replaceable>directory-names</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:267
msgid "Create directories."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:270
msgid "mv <replaceable>file1</replaceable> <replaceable>file2</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:272
msgid "Move, rename files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:275
msgid "rm <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:276
msgid "Remove files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:279
msgid "rmdir <replaceable>dirs</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:280
msgid "Remove empty directories."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:283
msgid ""
"tar <optional>c</optional><optional>x</optional><optional>t</"
"optional><optional>z</optional><optional>j</optional><optional>J</optional> -"
"f <replaceable>file</replaceable>.tar <optional><replaceable>files</"
"replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:287
msgid ""
"Create (c), extract (x), list table of (t)  archive file, <emphasis>z</"
"emphasis> for <filename>.gz</filename>, <emphasis>j</emphasis> for "
"<filename>.bz2</filename>, <emphasis>J</emphasis> for <filename>.xz</"
"filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:294
msgid "find <replaceable>directories expressions</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:296
msgid ""
"Find files like <literal>-name <replaceable>name</replaceable></literal> or "
"<literal>-size <replaceable>+1000</replaceable></literal>, etc."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:302
msgid ""
"grep <replaceable>search-string</replaceable> <replaceable>files</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:304
msgid "Find search-string in files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:307
msgid "ln -s <replaceable>file</replaceable> <replaceable>link</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:309
msgid "Create a symbolic link to a file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:313
msgid "ps <optional><replaceable>options</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:315
msgid "Show current processes."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:318
msgid ""
"kill <optional><replaceable>-9</replaceable></optional> <replaceable>PID</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:321
msgid ""
"Send signal to process (e.g. terminate it). Use <command>ps</command> for "
"PID."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:325
msgid "su - <optional><replaceable>username</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:327
msgid "Become another user, e.g. <filename>root</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:331
msgid "sudo <replaceable>command</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:332
msgid ""
"Execute a command as <filename>root</filename> as normal user, see "
"<filename>/etc/sudoers</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:337
msgid ""
"<replaceable>command</replaceable> <filename>&gt;</filename> "
"<replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:340
msgid "Overwrite file with output of command."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:344
msgid ""
"<replaceable>command</replaceable> <filename>&gt;&gt;</filename> "
"<replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:347
msgid "Append output of command to file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:351
msgid ""
"<replaceable>cmd1</replaceable> <filename>|</filename> <replaceable>cmd2</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:354
msgid "Use output of command 1 as input of command 2."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:358
msgid ""
"<replaceable>command</replaceable> <filename>&lt;</filename> "
"<replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:361
msgid "Use file as input for command."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:370
msgid "apt update"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:371
msgid ""
"Update packages listings from package repositories as listed in <filename>/"
"etc/apt/sources.list</filename>. Required whenever that file or the contents "
"of the repositories change."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:378
msgid "apt search <replaceable>search-string</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:380
msgid ""
"Search packages and descriptions for <replaceable>search-string</"
"replaceable>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:384
msgid "apt list -a <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:386
msgid "Show versions and archive areas of available packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:390
msgid "apt show -a <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:392
msgid "Show package information incl. description."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:396
msgid "apt install <replaceable>package-names</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:398
msgid "Install packages from repositories with all dependencies."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:402
msgid "apt upgrade"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:403
msgid "Install newest versions of all packages currently installed."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:407
msgid "apt full-upgrade"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:408
msgid ""
"Like <command>apt upgrade</command>, but with advanced conflict resolution."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:412
msgid "apt remove <replaceable>package-names</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:414
msgid "Remove packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:417
msgid "apt autoremove"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:418
msgid "Remove packages that no other packages depend on."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:422
msgid "apt depends <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:424
msgid "List all packages needed by the one given."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:428
msgid "apt rdepends <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:430
msgid "List all packages that need the one given."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:434
msgid "apt-file update"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:435
msgid ""
"Update content listings from package repositories, see <command>apt update</"
"command>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:440
msgid "apt-file search <replaceable>file-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:442
msgid "Search packages for file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:445
msgid "apt-file list <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:447
msgid "List contents of a package."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:450
msgid "aptitude"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:451
msgid "Console interface to APT, needs <filename>aptitude</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:455
msgid "synaptic"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:456
msgid "GUI interface to APT, needs <filename>synaptic</filename>."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:462
msgid "Dpkg"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:465
msgid "dpkg -l <optional><replaceable>names</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:467
msgid "List packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:470
msgid "dpkg -I <replaceable>pkg</replaceable>.deb"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:472
msgid "Show package information."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:475
msgid "dpkg -c <replaceable>pkg</replaceable>.deb"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:477
msgid "List contents of package file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:481
msgid "dpkg -S <replaceable>filename</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:483
msgid "Show which package a file belongs to."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:487
msgid "dpkg -i <replaceable>pkg</replaceable>.deb"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:489
msgid "Install package files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:492
msgid "dpkg -V <optional><replaceable>package-names</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:494
msgid "Audit check sums of installed packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:497
msgid ""
"dpkg-divert <optional>options</optional> <replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:499
msgid "Override a package&apos;s version of a file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:503
msgid ""
"dpkg <filename>--compare-versions </filename> <replaceable>v1</replaceable> "
"gt <replaceable>v2</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:506
msgid "Compare version numbers; view results with <command>echo $?</command>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:510
msgid ""
"dpkg-query -W &#x00A0;&#x00A0;&#x00A0;&#x00A0; <filename>--showformat</"
"filename>=<replaceable> format</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:512
msgid ""
"Query installed packages, format e.g. '${Package} ${Version} ${Installed-"
"Size}\\n'."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:517
msgid ""
"dpkg <filename>--get-selections</filename> &gt; <replaceable>file</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:519
msgid "Write package selections to file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:523
msgid ""
"dpkg <filename>--set-selections</filename> &lt; <replaceable>file</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:525
msgid "Set package selections from file."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:531
msgid "The Network"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:534
msgid "<filename>/etc/network/interfaces</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:535
msgid "Interface configuration (if not controlled via network-manager)."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:539
msgid ""
"ip link set <replaceable>device</replaceable> <optional>up</"
"optional><optional>down</optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:541
msgid "Start, stop network interfaces according to the file above."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:545
msgid "ip"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:546
msgid ""
"Show and manipulate network interfaces and routing, needs "
"<filename>iproute2</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:550
msgid "ssh -X <replaceable>user@host</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:552
msgid "Login at another machine."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:555
msgid ""
"scp <replaceable>files</replaceable> <replaceable>user@host:path</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:557
msgid "Copy files to other machine (and vice versa)."
msgstr ""
